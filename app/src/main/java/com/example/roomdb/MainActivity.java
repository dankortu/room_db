package com.example.roomdb;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    App app;
    AppDataBase db;
    EmployeeDao employeeDao;
    Disposable disposable;
    Disposable addEmployeeDisposable;
    Disposable updateEmployeeDisposable;

    final static List<Map<String, String>> employeesMapList = Arrays.asList(
            new HashMap<String, String>() {{
                put("name", "Карабас-барабас");
                put("salary", "3 ребенка");
            }},
            new HashMap<String, String>() {{
                put("name", "Тарзан");
                put("salary", "4 банана");
            }},
            new HashMap<String, String>() {{
                put("name", "Дамблдор");
                put("salary", "2 бузиных палочки");
            }},
            new HashMap<String, String>() {{
                put("name", "Дартаньян");
                put("salary", "3 шпаги");
            }}
    );

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        app = (App) getApplication();
        db = app.getDatabase();
        employeeDao = db.employeeDao();

        disposable = employeeDao.getAll().observeOn(AndroidSchedulers.mainThread()).subscribe(this::handleEmployeeList);

        Button changeSecondBtn = findViewById(R.id.change_second_employee);
        changeSecondBtn.setOnClickListener(this::onChangeSecondEmployee);


        Button addFourth = findViewById(R.id.add_fourth_employee);
        addFourth.setOnClickListener((View view) -> {
            view.setEnabled(false);
            addEmployeeById(3);
        });
    }

    private void onChangeSecondEmployee(View view) {
        if (updateEmployeeDisposable != null && !updateEmployeeDisposable.isDisposed()) {
            updateEmployeeDisposable.dispose();
        }
        updateEmployeeDisposable = employeeDao.update(new Employee(1, "Гомер", "3 банки пива")).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aVoid -> addEmployeeDisposable.dispose(), throwable -> {});
    }

    private void handleEmployeeList(List<Employee> employees) {
        if (employees.size() < 3) {
            addEmployeeById(employees.size());
        } else {
            employees.forEach(employee -> Log.i("EMPLOYEE", employee.id + " " + employee.name + " " + employee.salary));
        }
    }

    private void addEmployeeById(int id) {
        if (addEmployeeDisposable != null && !addEmployeeDisposable.isDisposed()) {
            addEmployeeDisposable.dispose();
        }

        addEmployeeDisposable = employeeDao.insert(new Employee(id, employeesMapList.get(id).get("name"), employeesMapList.get(id).get("salary")))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aVoid -> addEmployeeDisposable.dispose(), throwable -> {
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disposable.dispose();
    }
}

