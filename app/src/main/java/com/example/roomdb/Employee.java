package com.example.roomdb;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Employee {

    public Employee(long id, String name, String salary) {
        this.id = id;
        this.name = name;
        this.salary = salary;
    }

    @PrimaryKey
    public long id;

    public String name;

    public String salary;
}