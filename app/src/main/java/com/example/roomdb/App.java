package com.example.roomdb;

import android.app.Application;

import androidx.room.Room;

public class App extends Application {

    private AppDataBase database;

    @Override
    public void onCreate() {
        super.onCreate();
        database = Room.databaseBuilder(this, AppDataBase.class, "database")
                .build();
    }

    public AppDataBase getDatabase() {
        return database;
    }
}
