package com.example.roomdb;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Employee.class}, version = 1)
abstract class AppDataBase extends RoomDatabase {
    public abstract EmployeeDao employeeDao();
}
