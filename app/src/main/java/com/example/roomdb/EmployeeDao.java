package com.example.roomdb;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

@Dao
public interface EmployeeDao {

    @Query("SELECT * FROM employee")
    Flowable<List<Employee>> getAll();

    @Query("SELECT * FROM employee WHERE id = :id")
    Single<Employee> getById(long id);

    @Insert
    Single<Void> insert(Employee employee);

    @Update
    Single<Void> update(Employee employee);

    @Delete
    Single<Void> delete(Employee employee);

}
